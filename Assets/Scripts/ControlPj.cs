﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPj : MonoBehaviour
{
    public float aceleracion;
    private float velocidad;
    public float velocidad_max;
    private float _velocidad;
    public Vector2 fuerzaSalto; //La fuerza con la que salta
    private Transform miTransform;
    private Rigidbody2D miRigidbody;
    private Animator miAnimator;

    //controllar la colision con cualquier plataforma
    //private bool isGrounded;
    /*public LayerMask mascara;
    public Transform izqArriba, derAbajo;

    public Transform posShuriken;
    public GameObject miPoolObjectShuriken;*/
    
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        miTransform = this.transform;
        miRigidbody = GetComponent<Rigidbody2D>();
        miAnimator = GetComponent<Animator>();
    }

   /* private void FixedUpdate()
    {
        if(miRigidbody.velocity.y != 0)
        {
            isGrounded = Physics2D.OverlapArea(izqArriba.position, derAbajo.position, mascara);
        }
      
    }*/

    // Update is called once per frame
    void Update()
    {
        checkInput();
        accionesMario();
        //updateAnimaciones();    
    }

    private void checkInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            miRigidbody.AddForce(fuerzaSalto, ForceMode2D.Impulse);
        }
        if (Input.GetKey(KeyCode.A) && (_velocidad>-velocidad_max))
        {
            _velocidad = (float)((_velocidad*-aceleracion)-0.00001);
            miTransform.localScale = new Vector3(-1,1,1);
        }else if (Input.GetKey(KeyCode.D) && (_velocidad < velocidad_max))
        {
            _velocidad = (float)((_velocidad * aceleracion)+0.00001);
            miTransform.localScale = new Vector3(1, 1, 1);
        }
        if (Input.GetKey(KeyCode.F))
        {
            miAnimator.SetTrigger("Disparar");
        }
        Debug.Log(_velocidad);

    }

    private void accionesMario()
    {
        miTransform.Translate(Vector3.right * _velocidad *Time.deltaTime);
    }

    /*private void updateAnimaciones()
    {
        if (_velocidad < 0 || _velocidad > 0)
        {
            miAnimator.SetInteger("velocidad", 1);
        }else
        {
            miAnimator.SetInteger("velocidad", 0);
        }
        miAnimator.SetBool("isGrounded", isGrounded);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Plataforma_Movil"))
        {
            if (gameObject.GetComponent<SpriteRenderer>().bounds.min.y > collision.transform.position.y)
            {
                miTransform.parent = collision.transform;
            }
            miTransform.parent = collision.transform;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Plataforma_Movil"))
        {
            miTransform.parent = null;
        }
    }
    /*public void lanzarShurikens()
    {
        miPool
    }*/
}
